import numpy as np


def test_snapshot_numpy_1d(snapshot):
    snapshot.check(np.array([1, 2, 3, 4.0, 5.0]), float_format="{:.5f}")


def test_snapshot_object(snapshot):
    snapshot.check([1, 0, 3, 4])
