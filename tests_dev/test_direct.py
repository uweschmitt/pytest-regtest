#!/usr/bin/env python


import os
import tempfile
import time

import numpy as np
import pandas as pd
import pytest

from pytest_regtest import register_converter_post, register_converter_pre


@register_converter_post
def to_upper_conv(output):
    return output.upper()


@register_converter_pre
def noop(output, request):
    return output


here = os.path.abspath(__file__)


def test_regtest(regtest, tmpdir):
    with regtest:
        print("this is expected outcome")
        print(tmpdir.join("test").strpath)
        print(tempfile.gettempdir())
        print(tempfile.mkdtemp())
        print("obj id is", hex(id(here)))


def test_regtest_all(regtest_all, tmpdir):
    import sys

    print(sys.stdout)
    print(sys.__stdout__)
    print("this is expected outcome")
    print(tmpdir.join("test").strpath)
    print(tempfile.gettempdir())
    print(tempfile.mkdtemp())
    print("obj id is", hex(id(here)))


@pytest.mark.xfail
def test_always_fail():
    assert 1 * 1 == 2


@pytest.mark.xfail(strict=True)
def test_always_fail_regtest(regtest):
    regtest.write(str(time.time()))


@pytest.mark.xfail
def test_fail_regtest(regtest):
    regtest.write("hi")


def test_always_ok():
    assert 1 * 1 == 1


def test_always_ok_regtest(regtest):
    regtest.identifier = "my_computer"
    assert 1 * 1 == 1


@pytest.mark.parametrize("a, b, c", [(1, 2, 3), ("a", "b", "ab")])
def test_with_paramertrization(a, b, c, regtest):
    print(a, b, c, file=regtest)
    assert a + b == c


@pytest.mark.parametrize("a", ["123456." * 20])
def test_with_very_long_name(regtest, a):
    print("hi", file=regtest)
    print(repr(regtest), file=regtest)


@pytest.mark.xfail(strict=False)
def test_regtest_xfail(regtest_all, tmpdir):
    print("this is expected outcome")
    print(tmpdir.join("test").strpath)
    print(tempfile.gettempdir())
    print(tempfile.mkdtemp())
    print("obj id is", hex(id(tempfile)))


def test_snapshot(snapshot):
    print("hix", file=snapshot)
    snapshot.check(22)
    tt = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11] * 3
    tt = dict(a=tt, b=list(zip(tt, tt)))
    snapshot.check(tt, compact=False)
    snapshot.check("HI THERE!")

    data = np.eye(11) * 1.0002
    data[-1, -1] *= 1.1
    # data[1,2] = 1e-5
    snapshot.check(data, rtol=1e-6)

    df = pd.DataFrame(
        dict(
            a=[1 / 2, 1 / 3, None], b=[2, 3, 4], c=["aa", "bb", "cc"], d=[1.00001, 2, 3]
        )
    )
    snapshot.check(df, rtol=1e-6)
