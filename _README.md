![](https://gitlab.com/uweschmitt/pytest-regtest/badges/main/pipeline.svg)
![](https://gitlab.com/uweschmitt/pytest-regtest/badges/main/coverage.svg?job=coverage)


The full documentation for this package are available at
https://pytest-regtest.readthedocs.org

{% include docs/index.md %}
