# Taking snapshots of polars data frames

## Write a snapshot test for a data frame matrix

```python-code test_dataframe.py
import polars as pl
import numpy as np

def test_dataframe(snapshot):
    df = pl.DataFrame(np.eye(3), schema=["a", "b", "c"], orient="col")
    snapshot.check(df, atol=1e-2, rtol=1e-2)
```

## Run the test

The first execution of this test fails:

```bash-execute
$ pytest -v test_dataframe.py
```

## Reset the snapshot

Let us record the snapshot:

```bash-execute
$ pytest -v --regtest-reset test_dataframe.py
```

Now test test passes:

```bash-execute
$ pytest -v test_dataframe.py
```

## Break the test

```python-code test_dataframe.py
import polars as pl
import numpy as np

def test_dataframe(snapshot):
    df = pl.DataFrame(np.eye(3), schema=["a", "b", "d"], orient="col")
    snapshot.check(df, atol=1e-2, rtol=1e-2)
```

```bash-execute
$ pytest -v test_dataframe.py
```
