# The `regtest` fixture



## Write a test

The `pytest-regtest` plugin provides multiple fixtures.
To record output, use the fixture `regtest` that works like a file handle:

```python-code test_squares.py
def test_squares(regtest):

    result = [i*i for i in range(10)]

    # one way to record output:
    print(result, file=regtest)

    # alternative method to record output:
    regtest.write("done")
```

## Run the test

If you run this test script with *pytest* the first time there is no
recorded output for this test function so far and thus the test will
fail with a message including a diff:

```bash-execute
$ pytest -v test_squares.py
```

This is a diff of the current output `is` to a previously recorded output
`tobe`. Since we did not record output yet, the diff contains no lines marked
`+`.


## Reset the test

To record the current output, we run *pytest* with the *--regtest-reset*
flag:

```bash-execute
$ pytest -v --regtest-reset test_squares.py
```

You can also see from the output that the recorded output is in the
`_regtest_outputs` folder which in the same folder as the test script.
Don't forget to commit this folder to your version control system!

### Run the test again


```bash-execute
$ pytest -v test_squares.py
```

## Break the test

Let us break the test by changing the test function to compute
11 instead of 10 square numbers:

```python-code test_squares.py
def test_squares(regtest):

    result = [i*i for i in range(11)]

    # one way to record output:
    print(result, file=regtest)

    # alternative method to record output:
    regtest.write("done")
```


The next run of *pytest* delivers a nice diff of the current and expected
output from this test function:

```bash-execute
$ pytest -v test_squares.py
```

In case the change was intended, you can reset the test again:


```bash-execute
$ pytest -v --regtest-reset test_squares.py
```
