# Taking snapshots of pandas data frames

`pytest-regtest` implements snapshot testing for `pandas` data frames
`snapshot.check` offers options for managing numerical accuracies and
implements tailored diagnostics for failing tests.


## Write a snapshot test for a data frame

```python-code test_dataframe.py
import pandas as pd
import numpy as np

def test_dataframe(snapshot):
    df = pd.DataFrame(np.eye(3), columns=["a", "b", "c"])
    snapshot.check(df, atol=1e-2, rtol=1e-2)
```

## Run the test

The first execution of this test fails:

```bash-execute
$ pytest -v test_dataframe.py
```

## Reset the snapshot

Let us record the snapshot:

```bash-execute
$ pytest -v --regtest-reset test_dataframe.py
```

Now test test passes:

```bash-execute
$ pytest -v test_dataframe.py
```

## Break the test

We break the test by using the column name `d` instead of `c`:

```python-code test_dataframe.py
import pandas as pd
import numpy as np

def test_dataframe(snapshot):
    df = pd.DataFrame(np.eye(3), columns=["a", "b", "d"])
    snapshot.check(df, atol=1e-2, rtol=1e-2)
```

```bash-execute
$ pytest -v test_dataframe.py
```


## Changing the display options

`pytest-regtest` uses per default the display options from `pandas`.
Adapting print options can lead to more readable diagnostic
messages.

In the following example we create a data frame with 61 rows:


```python-code test_datafrae_with_printoptions.py
import pandas as pd
import numpy as np

def test_dataframe(snapshot):
    rows = 61
    matrix = np.arange(rows * 3).reshape(-1, 3) * 1.001
    df = pd.DataFrame(matrix, columns=["a", "b", "c"])

    snapshot.check(df)
```

The default settings of pandas truncate the display of data frames with
more than 60 rows:
```bash-execute
$ pytest -v test_datafrae_with_printoptions.py
```

To fix this we change the `max_rows` setting:

```python-code test_datafrae_with_printoptions.py
import pandas as pd
import numpy as np

def test_dataframe(snapshot):
    rows = 61
    matrix = np.arange(rows * 3).reshape(-1, 3) * 1.001
    df = pd.DataFrame(matrix, columns=["a", "b", "c"])

    # 'None" value means "unlimited":
    with pd.option_context("display.max_rows", None):
        snapshot.check(df)
```

Now we can see all rows:
```bash-execute
$ pytest -v test_datafrae_with_printoptions.py
```
