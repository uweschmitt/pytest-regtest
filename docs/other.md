# Other Features

### Using the `regtest` fixture as context manager

```python-code test_squares.py
def test_squares(regtest):

    result = [i*i for i in range(10)]

    with regtest:
        print(result)
```


### The `regtest_all` fixture

The `regtest_all` fixture leads to recording of all output to `stdout`
in a test function.

```python-code test_squares.py
def test_all(regtest_all):
    print("this line will be recorded.")
    print("and this line also.")
```

### Reset individual tests

You can reset recorded output of files and functions individually as:

```sh
$ pytest --regtest-reset test_demo.py
$ pytest --regtest-reset test_demo.py::test_squares
```

### Suppress diff for failed tests

To hide the diff and just show the number of lines changed, use:

```sh
$ py.test --regtest-nodiff ...
```


### Show all recorded output


For complex diffs it helps to see the full recorded output also.
To enable this use:

```sh
$ py.test --regtest-tee...
```


### Line endings

Per default `pytest-regtest` ignores different line endings in the
output. In case you want to disable this feature, use the
`-regtest-consider-line-endings` flag.


## Clean nondeterministic output before recording

Output can contain data which is changing from test run to test run,
e.g. paths created with the `tmpdir` fixture, hexadecimal object ids or
timestamps.

Per default the plugin helps to make output more deterministic by:

- replacing all temporary folder in the output with `<tmpdir...>` or
  similar markers, depending on the origin of the temporary folder
  (`tempfile` module, `tmpdir` fixture, ...)
- replacing hexadecimal values ` 0x...` of arbitrary length with
  `0x?????????`.

You can also implement your own cleanup routines as described below.

### Register own cleanup functions

You can register own converters in `conftest.py`:

```py
import re
import pytest_regtest

@pytest_regtest.register_converter_pre
def remove_password_lines(txt):
    '''modify recorded output BEFORE the default fixes
    like temp folders or hex object ids are applied'''

    # remove lines with passwords:
    lines = txt.splitlines(keepends=True)
    lines = [l for l in lines if "password is" not in l]
    return "".join(lines)

@pytest_regtest.register_converter_post
def fix_time_measurements(txt):
    '''modify recorded output AFTER the default fixes
    like temp folders or hex object ids are applied'''

    # fix time measurements:
    return re.sub(
        r"\d+(\.\d+)? seconds",
        "<SECONDS> seconds",
        txt
    )
```

If you register multiple converters they will be applied in the order of
registration.

In case your routines replace, improve or conflict with the standard
cleanup converters, you can use the flag `--regtest-disable-stdconv` to
disable the default cleanup procedure.

## Command line options summary

These are all supported command line options:

```bash-execute
$ pytest --help | grep regtest
```

## Implementing your own snapshot handlers

```include-python pytest_regtest.snapshot_handler.PythonObjectHandler
```

```include-python pytest_regtest.snapshot_handler.register_python_object_handler
```
