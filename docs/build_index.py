import os
import re
import subprocess
import tempfile

folder = tempfile.mkdtemp()

os.chdir(folder)


def code(txt):
    txt = txt.strip()
    print(
        f"""```py
{txt}
```"""
    )
    with open("test_squares.py", "w") as fh:
        fh.write(txt)
        fh.write("\n")


def run_test(command):
    print("```")
    print("$", command)
    output = subprocess.run(
        command, capture_output=True, text=True, shell=True
    ).stdout.rstrip()
    output = re.sub(r"platform (.*) -- .*\n", r"platform \1 -- ...\n", output)
    output = re.sub(r"rootdir: .*\n", r"rootdir: ...\n", output)
    print(output)
    print("```")


def run_help():
    print("```")
    print("$ pytest --help")
    output = (
        subprocess.run("pytest --help", capture_output=True, text=True, shell=True)
        .stdout.rstrip()
        .splitlines()
    )
    print("...")
    print()

    line_iter = iter(output)
    for line in line_iter:
        if line.startswith("regression test plugin:"):
            print(line)
            for line in line_iter:
                if not line.strip():
                    break
                print(line)
            break
    print("...")
    print("```")


print(
    """
# Home

## About

`pytest-regtest` is a plugin for [pytest](https://pytest.org) to implement
**regression testing**.

Unlike [functional testing](https://en.wikipedia.org/wiki/Functional_testing),
[regression testing](https://en.wikipedia.org/wiki/Regression_testing)
testing does not test whether the software produces the correct
results, but whether it behaves as it did before changes were introduced.

More specifically, `pytest-regtest` provides **snapshot testing**, which
implements regression testing by recording the textual output of a test
function and comparing this recorded output to a reference output.

**Regression testing** is a common technique to implement basic testing
before refactoring legacy code that lacks a test suite.

Snapshot testing can also be used to implement tests for complex outcomes, such
as recording textual database dumps or the results of a scientific analysis
routine.


## Installation

To install and activate this plugin execute:

    $ pip install pytest-regtest

## Basic Usage


### Write a test

The `pytest-regtest` plugin provides multiple fixtures.
To record output, use the fixture `regtest` that works like a file handle:
"""
)


code(
    """
def test_squares(regtest):

    result = [i*i for i in range(10)]

    # one way to record output:
    print(result, file=regtest)

    # alternative method to record output:
    regtest.write("done")
"""
)


print(
    """
You can also use the `regtest_all` fixture. This enables all output to stdout to be
recorded in a test function.


### Run the test

If you run this test script with *pytest* the first time there is no
recorded output for this test function so far and thus the test will
fail with a message including a diff:
"""
)


run_test("pytest -v test_squares.py")

print(
    """
This is a diff of the current output `is` to a previously recorded output
`tobe`. Since we did not record output yet, the diff contains no lines marked
`+`.


### Reset the test

To record the current output, we run *pytest* with the *--regtest-reset*
flag:
"""
)

run_test("pytest -v --regtest-reset test_squares.py")

print(
    """
You can also see from the output that the recorded output is in the
`_regtest_outputs` folder which in the same folder as the test script.
Don't forget to commit this folder to your version control system!

### Run the test again

When we run the test again, it succeeds:
      """
)

run_test("pytest -v test_squares.py")

print(
    """
### Break the test

Let us break the test by changing the test function to compute
11 instead of 10 square numbers:
      """
)

code(
    """
def test_squares(regtest):

    result = [i*i for i in range(11)]

    # one way to record output:
    print(result, file=regtest)

    # alternative method to record output:
    regtest.write("done")
"""
)


print(
    """
The next run of pytest delivers a nice diff of the current and expected output
from this test function:
"""
)

run_test("pytest -v test_squares.py")

print(
    """

## Other features

### Using the `regtest` fixture as context manager

The `regtest` fixture also works as a context manager  to capture
all output from the wrapped code block:
    """
)

code(
    """
def test_squares(regtest):

    result = [i*i for i in range(10)]

    with regtest:
        print(result)
"""
)


print(
    """
### The `regtest_all` fixture

The `regtest_all` fixture leads to recording of all output to `stdout` in a
test function.
      """
)

code(
    """
def test_all(regtest_all):
    print("this line will be recorded.")
    print("and this line also.")
"""
)

print(
    r"""
### Reset individual tests

You can reset recorded output of files and functions individually as:

```sh
$ py.test --regtest-reset test_demo.py
$ py.test --regtest-reset test_demo.py::test_squares
```

### Suppress diff for failed tests

To hide the diff and just show the number of lines changed, use:

```sh
$ py.test --regtest-nodiff ...
```


### Show all recorded output


For complex diffs it helps to see the full recorded output also.
To enable this use:

```sh
$ py.test --regtest-tee...
```


### Line endings

Per default `pytest-regtest` ignores different line endings in the output.
In case you want to disable this feature, use the `-regtest-consider-line-endings`
flag.


## Clean indeterministic output before recording

Output can contain data which is changing from test run to test
run, e.g. paths created with the `tmpdir` fixture, hexadecimal object ids or
timestamps.

Per default the plugin helps to make output more deterministic by:

- replacing all temporary folder in the output with `<tmpdir...>` or similar markers,
  depending on the origin of the temporary folder (`tempfile` module, `tmpdir` fixture,
   ...)
- replacing hexadecimal values ` 0x...` of arbitary length by `0x?????????`.

You can also implement your own cleanup routines as described below.

### Register own cleanup functions

You can register own converters in `conftest.py`:

```py
import re
import pytest_regtest

@pytest_regtest.register_converter_pre
def remove_password_lines(txt):
    '''modify recorded output BEFORE the default fixes
    like temp folders or hex object ids are applied'''

    # remove lines with passwords:
    lines = txt.splitlines(keepends=True)
    lines = [l for l in lines if "password is" not in l]
    return "".join(lines)

@pytest_regtest.register_converter_post
def fix_time_measurements(txt):
    '''modify recorded output AFTER the default fixes
    like temp folders or hex object ids are applied'''

    # fix time measurements:
    return re.sub(
        r"\d+(\.\d+)? seconds",
        "<SECONDS> seconds",
        txt
    )
```

If you register multiple converters they will be applied in the order of
registration.

In case your routines replace, improve or conflict with the standard cleanup converters,
you can use the flag `--regtest-disable-stdconv` to disable the default cleanup
procedure.

## Command line options summary

These are all supported command line options:
    """
)

run_help()
