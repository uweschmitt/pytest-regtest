# Snapshots

For non-textual data structures you can use the `snapshot` fixture
instead of the `regtest` fixtures.

This function depends on the data type of the `snapshot.check`
argument and `pytest-regtest` ships with implementations for:

- Python objects, such as dictionaries or lists
- `NumPy` arrays of arbitrary dimensions.
- `pandas` data frames.
- `polars` data frames.

The user can provide own handlers for particular data structures.


## Write a snapshot test for a nested Python data structure

For non-textual data structures you can use the `snapshot` fixture
instead of the `regtest` fixture.
This fixture offers a method `.check`:


```python-code test_snapshot.py
def test_nested_data(snapshot):
    li = list(range(5))
    text = "lorem ipsum " * 8
    data = dict(li=li, text=text)
    snapshot.check(data)
```


## Run the test

If you run this test script with *pytest* the first time there is no
recorded output for this test function so far and thus the test will
fail with a message including a diff:

```bash-execute
$ pytest -v test_snapshot.py
```

This is a diff of the current output `is` to a previously recorded output
`tobe`. Since we did not record output yet, the diff contains no lines marked
`+`.


## Reset the test

To record the current output, we run *pytest* with the *--regtest-reset*
flag:

```bash-execute
$ pytest -v --regtest-reset test_snapshot.py
```

You can also see from the output that the recorded output is in the
`_regtest_outputs` folder which in the same folder as the test script.
Don't forget to commit this folder to your version control system!

## Run the test again


```bash-execute
$ pytest -v test_snapshot.py
```

## Break the test

Let us break the test by changing the length of `li` from
5 to 6 and the number of repetitions when constructing `text` from 8 to
7:

```python-code test_snapshot.py
def test_nested_data(snapshot):
    li = list(range(6))
    dd = dict(li=li)
    text = "lorem ipsum " * 7
    data = dict(li=li, text=text)
    snapshot.check(data)
```


The next run of *pytest* delivers a nice diff of the current and
expected output from this test function:

```bash-execute
$ pytest -v test_snapshot.py
```

In case the change was intended, you can reset the test again:


```bash-execute
$ pytest -v --regtest-reset test_snapshot.py
```
