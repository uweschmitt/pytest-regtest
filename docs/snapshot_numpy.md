# Taking snapshots of NumPy arrays

`pytest-regtest` implements snapshot testing for `NumPy` arrays of
arbitrary dimension. `snapshot.check` offers
options for managing numerical accuracies and implements
tailored diagnostics for failing tests.

## Write a snapshot test for a NumPy matrix

The following test checks a 3 times 3 matrix:

```python-code test_squares.py
import numpy as np

def test_squares(snapshot):
    result = np.arange(9.0).reshape(3, 3)
    snapshot.check(result)
```


## Run the test

If you run this test script with *pytest* the first time there is no
recorded output for this test function so far and thus the test will
fail with a message including a diff:

```bash-execute
$ pytest -v test_squares.py
```

This is a diff of the current output `is` to a previously recorded output
`tobe`. Since we did not record output yet, the diff contains no lines marked
`+`.


## Reset the test

To record the current output, we run *pytest* with the *--regtest-reset*
flag:

```bash-execute
$ pytest -v --regtest-reset test_squares.py
```

You can also see from the output that the recorded output is in the
`_regtest_outputs` folder which in the same folder as the test script.
Don't forget to commit this folder to your version control system!

## Run the test again


```bash-execute
$ pytest -v test_squares.py
```

## Break the test

Let us break the test by changing the test function to compute
11 instead of 10 square numbers:

```python-code test_squares.py
import numpy as np

def test_squares(snapshot):
    result = np.arange(12.0).reshape(4, 3)
    result[:, 0] += 1
    snapshot.check(result)
```


The next run of *pytest* delivers a nice diff of the current and
expected output from this test function:

```bash-execute
$ pytest -v test_squares.py
```

In case the change was intended, you can reset the test again:


```bash-execute
$ pytest -v --regtest-reset test_squares.py
```


## Testing with numerical tolerances

The snapshot implementation for `NumPy` arrays also supports
absolute and numerical tolerances using the optional arguments
`rtol` and `atol` in the `snapshot.check` call. If not explicitly
specified, both parameters are set to `0.0` which is often to prudent
and must be adapted by the user:


```python-code test_squares_with_tolerance.py
import numpy as np

def test_squares(snapshot):
    result = np.arange(9.0).reshape(3, 3)
    snapshot.check(result, atol=1e-8, rtol=1e-6)
```

We record this 3 x 3 matrix:

```bash-execute
$ pytest -v --regtest-reset test_squares_with_tolerance.py
```


Now we change our data within the specified limits:

```python-code test_squares_with_tolerance.py
import numpy as np

def test_squares(snapshot):
    result = np.arange(9.0).reshape(3, 3) + 1e-8
    snapshot.check(result, atol=1e-8, rtol=1e-6)
```

As we can see, the test still passes:

```bash-execute
$ pytest -v test_squares_with_tolerance.py
```


## Changing the formatting of numbers

`pytest-regtest` uses per default the output settings from `NumPy`.
Adapting print options can lead to more readable diagnostic
messages:

```python-code test_squares_with_printoptions.py
import numpy as np

def test_squares(snapshot):
    result = np.arange(9.0).reshape(3, 3) - 2.0
    with np.printoptions(formatter=dict(float="{:+.3e}".format)):
        snapshot.check(result)
```

You can see that the numbers are not formatted with `{+.3e}` which
forces scientific notation with 3 digits after the decimal point and
always shows the sign of the numbers:

```bash-execute
$ pytest -v test_squares_with_printoptions.py
```
