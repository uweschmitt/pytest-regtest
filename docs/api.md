# API

## pytest-regtest public API

::: pytest_regtest.register_converter_post

::: pytest_regtest.register_converter_pre

::: pytest_regtest.clear_converters

## Snapshot handler API


::: pytest_regtest.snapshot_handler.SnapshotHandlerRegistry


::: pytest_regtest.snapshot_handler.BaseSnapshotHandler
