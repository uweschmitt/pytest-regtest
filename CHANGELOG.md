# Version 2.3.3

- Fix empty python package

# Version 2.3.2

- Fix bug when persisted snapshot was stored by different handler / in
  different format.

# Version 2.3.1

- Fixes to support Python 3.9

# Version 2.3.0

- added snapshot handlers for polars data frames (@jonasmo1)

# Version 2.2.0

- added snapshot handlers for python objects, NumPy arrays and pandas
  data frames (@uweschmitt).
