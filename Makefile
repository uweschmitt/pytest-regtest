.PHONY: docs clean all

all:
	@echo
	@echo "docs:     build docs"
	@echo "release:  build package and upload to pypi"

docs:
	uv run python -m md_transformer.cli _README.md > README.md
	uv run mkdocs build

release:
	python -m build -w . \
  	&& twine check dist/*.whl \
  	&& twine upload dist/*.whl
